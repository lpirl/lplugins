import
  ../lib/lv2-nim/lv2-nim/lv2,
  ../lib/lplugins


type
  CSample = cfloat
  CSamples = UncheckedArray[CSample]
  PortIndex = enum
    LGAIN_INPUT = 0,
    LGAIN_OUTPUT = 1,
    LGAIN_GAIN = 2
  LGain = object
    input: ptr CSamples
    output: ptr CSamples
    gain: ptr cfloat


proc instantiate(descriptor: ptr Lv2Descriptor;
                  sampleRate: cdouble; bundlePath: cstring;
                  features: ptr ptr Lv2Feature): Lv2Handle {.cdecl.} =
  return createShared(LGain)


func connectPort(instance: Lv2Handle; port: cuint32;
                  dataLocation: pointer) {.cdecl.} =
  let lgain1 = cast[ptr LGain](instance)
  case cast[PortIndex](port)
  of LGAIN_INPUT:
    lgain1.input = cast[ptr CSamples](dataLocation)
  of LGAIN_OUTPUT:
    lgain1.output = cast[ptr CSamples](dataLocation)
  of LGAIN_GAIN:
    lgain1.gain = cast[ptr cfloat](dataLocation)


func activate(instance: Lv2Handle) {.cdecl.} =
  discard


func run(instance: Lv2Handle; nCSamples: cuint32) {.cdecl.} =
  let lgain1 = cast[ptr LGain](instance)
  for pos in 0 ..< nCSamples:
    lgain1.output[pos] = lgain1.input[pos] * db2coeff(lgain1.gain[])


func deactivate(instance: Lv2Handle) {.cdecl.} =
  discard


proc cleanup(instance: Lv2Handle) {.cdecl.} =
  freeShared(cast[ptr LGain](instance))


func extensionData(uri: cstring): pointer {.cdecl.} =
  return nil


proc lv2Descriptor(index: cuint32): ptr Lv2Descriptor
                   {.cdecl, exportc, dynlib, extern: "lv2_descriptor".} =
  case index
  of 0:
    result = createShared(Lv2Descriptor)
    result.uri = "https://gitlab.com/lpirl/lplugins#lgain1"
    result.instantiate = instantiate
    result.connectPort = connectPort
    result.activate = activate
    result.run = run
    result.deactivate = deactivate
    result.cleanup = cleanup
    result.extensionData = extensionData
  else:
    result = nil
