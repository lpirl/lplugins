from math import pow, log10

func db2coeff*(db: cfloat): cfloat =
  pow(10, db / 20)

func coeff2db*(coeff: cfloat): cfloat =
  20 * log10(coeff)
