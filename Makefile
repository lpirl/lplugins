#!/usr/bin/make

SRCDIR=src
BINDIR=bin

PLUGINS ?= $(filter-out lib, $(notdir $(wildcard $(SRCDIR)/*)))

NIMC ?= nim c

ifndef NIMCFLAGS

	# flags for compiling a library:
	NIMCFLAGS += --app:lib
	NIMCFLAGS += --noMain:on

	# real-time-safe (/!\ but not cycle-safe) garbage collection
	NIMCFLAGS += --gc:arc

	# LV2 hosts might run the same plugin in different threads, so
	# thread-local storage makes no sense (does it?)
	NIMCFLAGS += -d:tlsEmulation=off

endif

ifdef DEBUG
	NIMCFLAGS += --passC:-g3 --listCmd
else
	NIMCFLAGS += -d:release --opt:speed --assertions:off

	# https://gcc.gnu.org/onlinedocs/gcc/Optimize-Options.html
	NIMCFLAGS += --passC:-Ofast --passC:-fno-finite-math-only --passC:-flto
endif


define PLUGIN_template =
  $(1): $(BINDIR)/$(1) \
        $(BINDIR)/$(1)/lplugins.ttl \
        $(BINDIR)/$(1)/manifest.ttl \
        $(BINDIR)/$(1)/$(1).so

endef

$(foreach plugin,$(PLUGINS),$(eval $(call PLUGIN_template,$(plugin))))


all: $(PLUGINS)

list:
	@echo $(PLUGINS) | tr ' ' '\n'

$(BINDIR)/%:
	mkdir -p $@

$(BINDIR)/%/lplugins.ttl: $(SRCDIR)/lib/lplugins.ttl
	cp $^ $@

$(BINDIR)/%.ttl: $(SRCDIR)/%.ttl
	cp $^ $@

$(BINDIR)/%.so: $(SRCDIR)/%.nim
	$(NIMC) $(NIMCFLAGS) -o:$@ $^

clean:
	rm -rf $(BINDIR)
