.. image:: https://gitlab.com/lpirl/lplugins/badges/main/pipeline.svg
  :target: https://gitlab.com/lpirl/lplugins/pipelines
  :align: right

lplugins
========

* an experimental set of "minimal" plugins

  * no meters

    * forces to actually listen

  * output gain for level matching

    * except where absolutely pointless (e.g., gain plugin)

  * no "composite" plugins

    * e.g., EQ with ``n`` bands,

      * would suggest to use ``n`` bands
      * What, if you need ``n-1``?
      * What, if you need ``n+1``?
      * use ``n`` single-band EQs instead

  * no custom graphical interfaces
  * no optimization for average or corner cases

    * pro: no oblique assumptions about average usage
    * pro: plugin resource usage is (in that regard) independent from
      input
    * pro: keeps code more straight-forward
    * con: might wastes resources in (maybe actually existing) average
      cases

* written in Nim

  * for greater productivity (hopefully :))
  * out of curiosity

* number suffixes in plugin names

  * so changes to don't break sessions (or make them sound different)


quick start
-----------

Download and load in a LV2 host:

.. code:: shell

  wget -O lplugins.tar.gz 'https://gitlab.com/lpirl/lplugins/-/jobs/artifacts/main/raw/lplugins.tar.gz?job=archive'
  tar xaf lplugins.tar.gz

  # start e.g. Ardour as LV2 host:
  LV2_PATH=$(readlink -f lplugins) Ardour6


Development
-----------

Install dependencies and compile:

.. code:: shell

  sudo apt install make nim
  make

To compile debug builds, you can run:

.. source:: shell

  make DEBUG=1

Development currently happens on Debian, feel free to contributed fixes
for other distributions or platforms.

Also, I am no expert in Nim or DSP, so any suggestions regarding
DSP, syntax, semantics, or conventions are more than welcome.

.. ~ algorithms / bases
.. ~ ..................

.. ~ Stolen algorithms:

.. ~ =============== =========
.. ~ component       basis
.. ~ =============== =========
.. ~ foo             https://example.com/foo
.. ~ =============== =========
